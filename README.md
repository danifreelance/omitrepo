hotelbooking
============

A Symfony project with a list of hotels. The user can check the availability of a room and then book it.

General Info
-----------------------
Symfony 2.8.2 version

Instructions
-----------------------

- Fork this repository
- install composer and project dependencies
- Execute php app/console doctrine:schema:update --force
- Execute php app/console doctrine:fixtures:load
- Run project

Make a reservation
-----------------------

- Select hotel, dates and submit
- If rooms are available, select your room
- If ajax call is successfull, you can see your reservation on the list of hotels by choosing your hotel and checking the dates in your room