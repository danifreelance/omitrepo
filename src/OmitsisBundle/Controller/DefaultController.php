<?php

namespace OmitsisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OmitsisBundle\Entity\Hotel;
use OmitsisBundle\Entity\Room;
use OmitsisBundle\Entity\Bookings;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $d = array();
        
        $form = $this->createFormBuilder()
            ->add('hotelid','entity',array(
                'label'         => 'Hotel',
                'class'         =>'OmitsisBundle:Hotel',
                'choice_label' => function ($obj) {
                        $stars = "";
                        for($i=1; $i<= $obj->getStars(); $i++){
                            $stars .= "*";
                        }
                        return $obj->getName() .' '. $stars;
                    },
                'choice_value' => function ($obj) {
                        return $obj->getId();
                    },
            ))
            ->add('datein','date',array('data'=>new \DateTime('+0 days'),'label'=>'Check In', 'widget' => 'single_text', 'format' => 'dd-MM-yyy'))
            ->add('dateout','date',array('data'=>new \DateTime('+1 days'),'label'=>'Check Out', 'widget' => 'single_text', 'format' => 'dd-MM-yyy'))
            ->add('send', 'submit',array('label'=>'Check Availability'))
            ->getForm();

        $form->handleRequest($request);

        if($form->isValid()) {
            
            $d['idHotel'] = $form->get('hotelid')->getData()->getId();
            $d['dateIn'] = $form->get('datein')->getData();
            $d['dateOut'] = $form->get('dateout')->getData();
            $d['rooms'] = array();
            
            // check valid dates
            if($d['dateIn'] >= $d['dateOut']){
                throw $this->createNotFoundException('Check In must be previous than Check out');
            }
            
            if($d['dateIn'] < new \DateTime('today') || $d['dateOut'] <= new \DateTime('today')){
                throw $this->createNotFoundException('Check In and Check out must be future dates');
            }

            //check availability rooms for this checkin and checkout
            $em = $this->getDoctrine()->getManager();
            
            $q = "SELECT book.idhotel, book.idroom, book.datein, book.dateout
                    FROM OmitsisBundle:Bookings book
                    WHERE book.idhotel = :idhotel
                        AND (book.datein >= :datein AND book.datein < :dateout OR book.dateout > :datein AND book.dateout <= :dateout)
                    GROUP BY book.idroom";
            $query = $em->createQuery($q);
            $query->setParameter('idhotel', $d['idHotel']);
            $query->setParameter('datein', $d['dateIn']);
            $query->setParameter('dateout', $d['dateOut']);
            
            $result = $query->getArrayResult();
            
            // get only idrooms
            $idRooms = array_column($result, 'idroom');
                                 
            // get all rooms by idhotel
            $qRooms = $em->getRepository('OmitsisBundle:Room')->findBy(array(
                'hotel' => $d['idHotel']
            ));
            
            //create a new array with available rooms (add if room not exists in $idRooms array)
            foreach($qRooms as $room){
                if(!in_array($room->getId(),$idRooms)){
                    $d['rooms'][$room->getId()] = $room->getNumber();
                }
            }
            

        }
        
        return $this->render('OmitsisBundle:Default:home.html.twig',array(
            'form'=> $form->createView(),
            'data' => $d,
            'isHome' => true,
            'test' => true
        ));
    }
    
    /**
     * @Route("/listhotels", name="listhotels")
     */
    public function listHotelsAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        // get all rooms by idhotel
        $hotels = $em->getRepository('OmitsisBundle:Hotel')->findAll();
        
        return $this->render('OmitsisBundle:Default:listHotels.html.twig',array(
            'hotels' => $hotels,
            'isHome' => false
        ));
    }
    
    /**
     * @Route("/hotel/{hotelid}", name="hotelInfo")
     * @Template()
     */
    public function hotelInfoAction($hotelid)
    {
        $em = $this->getDoctrine()->getManager();
        
        // get all rooms by idhotel
        $hotel = $em->getRepository('OmitsisBundle:Hotel')->findOneById($hotelid);
        
        $q = 'SELECT b.idroom, b.datein, b.dateout, r.number FROM OmitsisBundle:Bookings b JOIN OmitsisBundle:Room r WITH b.idroom = r.id WHERE b.idhotel = :idhotel ORDER BY b.idroom ASC, b.datein ASC';
        $query = $em->createQuery($q);
        $query->setParameter('idhotel', $hotelid);
        $bookings = $query->getResult();
        
        $hotelBookings = array();
        
        foreach($bookings as $book){
            
            $hotelBookings[$book['idroom']][] = $book;            
            
        }
                        
        return $this->render('OmitsisBundle:Default:hotelInfo.html.twig',array(
            'hotel' => $hotel,
            'hBookings' => $hotelBookings
        ));
    }
    
    /**
     * @Route("/saveRoom", name="saveRoom")
     * @Template()
     */
    public function saveRoomAction()
    {
        if(!$this->getRequest()->isXmlHttpRequest()){
            throw $this->createNotFoundException('Invalid request');
        }
        
        $request = $this->container->get('request');  
        $idhotel = $request->request->get('idhotel');
        $idroom = $request->request->get('idroom');
        $dateIn = $request->request->get('dateIn');
        $dateOut = $request->request->get('dateOut');
        
        $em = $this->getDoctrine()->getManager();
        
        $booking = new Bookings();
        
        $booking->setIdhotel($idhotel);
        $booking->setIdroom($idroom);
        $booking->setDatein(new \DateTime($dateIn));
        $booking->setDateout(new \DateTime($dateOut));
        
        try{
            $em->persist($booking);            
        } catch (Exception $e) {
            throw $this->createNotFoundException('Error saving the booking. Please try again');
        }
        
        $em->flush();
        $em->clear();        
        
        return $this->render('OmitsisBundle:Default/ajax:roomReservation.html.twig');
    }
    
}
