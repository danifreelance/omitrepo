<?php

namespace OmitsisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bookings
 *
 * @ORM\Table(name="bookings")
 * @ORM\Entity(repositoryClass="OmitsisBundle\Repository\BookingsRepository")
 */
class Bookings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idhotel", type="integer")
     */
    private $idhotel;

    /**
     * @var int
     *
     * @ORM\Column(name="idroom", type="integer")
     */
    private $idroom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datein", type="date")
     */
    private $datein;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateout", type="date")
     */
    private $dateout;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idhotel
     *
     * @param integer $idhotel
     * @return Bookings
     */
    public function setIdhotel($idhotel)
    {
        $this->idhotel = $idhotel;

        return $this;
    }

    /**
     * Get idhotel
     *
     * @return integer 
     */
    public function getIdhotel()
    {
        return $this->idhotel;
    }

    /**
     * Set idroom
     *
     * @param integer $idroom
     * @return Bookings
     */
    public function setIdroom($idroom)
    {
        $this->idroom = $idroom;

        return $this;
    }

    /**
     * Get idroom
     *
     * @return integer 
     */
    public function getIdroom()
    {
        return $this->idroom;
    }

    /**
     * Set datein
     *
     * @param \DateTime $datein
     * @return Bookings
     */
    public function setDatein($datein)
    {
        $this->datein = $datein;

        return $this;
    }

    /**
     * Get datein
     *
     * @return \DateTime 
     */
    public function getDatein()
    {
        return $this->datein;
    }

    /**
     * Set dateout
     *
     * @param \DateTime $dateout
     * @return Bookings
     */
    public function setDateout($dateout)
    {
        $this->dateout = $dateout;

        return $this;
    }

    /**
     * Get dateout
     *
     * @return \DateTime 
     */
    public function getDateout()
    {
        return $this->dateout;
    }
}
